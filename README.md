# DEPRECATED

Since we switched to using the official [Kubernetes Go Client](https://github.com/kubernetes/client-go), this client is not maintained anymore!





Introduction
======

Amdatu Kubernetes Go is a client library for the Kubernetes API in Go.
It has very limited external dependencies, so that it can more easily be used in a project than the official Go client.
The library currently supports the most important API operations, but not all.
If you're missing any operations, please create an issue or provide a pull request to make the library more useful for everyone.
The type definitions for resources used in the API are copied from the Kubernetes project.
Some minor changes have been made to cut a few more dependencies, but in general everything in the `api` package is coming from Kubernetes itself.

Using the library
========

API documentation is available in [godoc](https://godoc.org/bitbucket.org/amdatulabs/amdatu-kubernetes-go/client).

```
package main

import (
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/client"
	"log"
)

const KUBERNETES_URL = "http://10.150.16.32:8080"
const TEST_NAMESPACE = "client-test"
var kubernetes = client.NewClient(KUBERNETES_URL, "", "")

func main() {
	createRc()
	listPods()
}

func createRc() {
	replicas := int32(1)
	labels := map[string]string{"name": "test"}
	rc := v1.ReplicationController{
		ObjectMeta: v1.ObjectMeta{Name: "test", Namespace: TEST_NAMESPACE, Labels: labels},
		Spec: v1.ReplicationControllerSpec{
			Selector: map[string]string{"name": "test"},
			Replicas: &replicas,
			Template: &v1.PodTemplateSpec{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{"name": "test"},
				},

				Spec: v1.PodSpec{
					Containers: []v1.Container{{
						Name:  "nginx",
						Image: "nginx",
					}},
				},
			},
		},
	}

	createdRc, err := kubernetes.CreateReplicationController(TEST_NAMESPACE, &rc)

	if err != nil {
		panic(err)
	}

	log.Printf("Created replication controller %v\n", createdRc.Name)
}

func listPods() {
	pods, err := kubernetes.ListPods(TEST_NAMESPACE)
	if err != nil {
		panic(err)
	}

	for _,pod := range pods.Items {
		log.Println(pod.Name)
	}
}
```

The [test code](https://bitbucket.org/amdatulabs/amdatu-kubernetes-go/src/master/client/client_test.go?at=master&fileviewer=file-view-default) is also a good way to explore the API.
