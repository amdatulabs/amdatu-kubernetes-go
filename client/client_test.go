/*
Copyright (c) 2016 The Amdatu Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package client

import (
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"
	"log"
	"os"
	"testing"
	"time"
)

var KUBERNETES_URL string

const TEST_NAMESPACE = "test"

var client Client

func TestMain(m *testing.M) {
	KUBERNETES_URL = os.Getenv("KUBERNETES_URL")
	if KUBERNETES_URL == "" {
		log.Fatal("KUBERNETES_URL not set")
	}

	log.Printf("Configured KUBERNETES_URL to %v\n", KUBERNETES_URL)
	client = NewClient(KUBERNETES_URL, "", "")

	os.Exit(m.Run())
}

func TestCreateBasicAuthHeader(t *testing.T) {
	header := createBasicAuthHeader("someuser", "somepassword")
	if header != "Bearer c29tZXVzZXI6c29tZXBhc3N3b3Jk" {
		t.Error("Basic auth header value incorrect")
	}
}

func TestCreateNamespace(t *testing.T) {

	client.DeleteNamespace(TEST_NAMESPACE)

	time.Sleep(1 * time.Second)

	result, err := client.CreateNamespace(TEST_NAMESPACE)
	if err != nil {
		t.Error(err)
	}

	if result.Name != TEST_NAMESPACE {
		t.Error("Wrong namespace name")
	}
}

func TestWatchPods(t *testing.T) {

	labels := map[string]string{"name": "test"}
	events, _, err := client.WatchPodsWithLabel(TEST_NAMESPACE, labels)

	if err != nil {
		t.Error(err)
	}

	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(2 * time.Second)
		timeout <- true
	}()

	result, err := createRc()
	if err != nil {
		t.Error(err)
	}

	select {
	case evnt := <-events:
		log.Println(evnt.Object.Name)
		client.DeleteReplicationController(TEST_NAMESPACE, result.Name)

	case <-timeout:
		t.Error("Timeout")
		client.DeleteReplicationController(TEST_NAMESPACE, result.Name)
	}
}

func TestCreateAndUpdateReplicationController(t *testing.T) {
	result, err := createRc()
	if err != nil {
		t.Error(err)
	}

	if result.Name != "test" {
		t.Error("Replication Controller not created")
	}

	rc, err := client.GetReplicationController(TEST_NAMESPACE, result.Name)
	if err != nil {
		t.Error(err)
	}

	rc.Labels["updated"] = "update"
	updated, err := client.UpdateReplicationController(TEST_NAMESPACE, rc)

	if err != nil {
		t.Error(err)
	}

	if updated.Labels["updated"] != "update" {
		t.Error("Replication Controller not updated")
	}
}

func createRc() (*v1.ReplicationController, error) {
	replicas := int32(1)
	labels := map[string]string{"name": "test"}
	rc := v1.ReplicationController{
		ObjectMeta: v1.ObjectMeta{Name: "test", Namespace: TEST_NAMESPACE, Labels: labels},
		Spec: v1.ReplicationControllerSpec{
			Selector: map[string]string{"name": "test"},
			Replicas: &replicas,
			Template: &v1.PodTemplateSpec{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{"name": "test"},
				},

				Spec: v1.PodSpec{
					Containers: []v1.Container{{
						Name:  "nginx",
						Image: "nginx",
					}},
				},
			},
		},
	}

	return client.CreateReplicationController(TEST_NAMESPACE, &rc)

}

func TestListPods(t *testing.T) {

	result, err := client.ListPods(TEST_NAMESPACE)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("No pods returned")
	}
}

func TestListPodsNoNamespace(t *testing.T) {

	result, err := client.ListPods("")
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) == 0 {
		t.Error("No pods returned")
	}
}

func TestListPodsWithLabel(t *testing.T) {
	labels := map[string]string{"name": "other"}
	result, err := client.ListPodsWithLabel(TEST_NAMESPACE, labels)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 0 {
		t.Error("Pods not filtered correctly")
	}

	labels = map[string]string{"name": "test"}
	result, err = client.ListPodsWithLabel(TEST_NAMESPACE, labels)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("Pod not found")
	}
}

func TestGetAndUpdatePod(t *testing.T) {
	pods, err := client.ListPods(TEST_NAMESPACE)
	if err != nil {
		t.Error(err)
	}

	pod, err := client.GetPod(TEST_NAMESPACE, pods.Items[0].Name)
	if err != nil {
		t.Error(err)
	}

	if pod.Name != pods.Items[0].Name {
		t.Error("Wrong pod retrieved")
	}

	pod.Labels["updated"] = "update"
	updated, err := client.UpdatePod(TEST_NAMESPACE, pod)

	if err != nil {
		t.Error(err)
	}

	if updated.Labels["updated"] != "update" {
		t.Error("Pod not updated")
	}
}

func TestListReplicationControllers(t *testing.T) {

	result, err := client.ListReplicationControllers(TEST_NAMESPACE)

	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("No replication controllers returned")
	}
}

func TestGetReplicationControllers(t *testing.T) {

	result, err := client.GetReplicationController(TEST_NAMESPACE, "test")

	if err != nil {
		t.Error(err)
	}

	if result.Name != "test" {
		t.Error("No replication controllers returned")
	}
}

func TestListReplicationControllersWithLabel(t *testing.T) {
	labels := map[string]string{"name": "nofound"}

	result, err := client.ListReplicationControllersWithLabel(TEST_NAMESPACE, labels)

	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 0 {
		t.Error("Replication controllers not filtered")
	}

	labels = map[string]string{"name": "test"}

	result, err = client.ListReplicationControllersWithLabel(TEST_NAMESPACE, labels)

	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("Replication controllers not found")
	}
}

func TestCreateService(t *testing.T) {
	labels := map[string]string{"name": "test"}

	service := v1.Service{
		ObjectMeta: v1.ObjectMeta{Namespace: TEST_NAMESPACE, Name: "testservice", Labels: labels},
		Spec: v1.ServiceSpec{
			Selector: map[string]string{"name": "test"},
			Ports: []v1.ServicePort{{
				Port: 8080,
			}},
		},
	}

	_, err := client.CreateService(TEST_NAMESPACE, &service)

	if err != nil {
		t.Error(err)
	}
}

func TestListServices(t *testing.T) {
	result, err := client.ListServices(TEST_NAMESPACE)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("Service not found")
	}
}

func TestGetServices(t *testing.T) {
	result, err := client.GetService(TEST_NAMESPACE, "testservice")
	if err != nil {
		t.Error(err)
	}

	if result.Name != "testservice" {
		t.Error("Service not found")
	}
}

func TestListServicesWithLabel(t *testing.T) {

	labels := map[string]string{"name": "notfound"}

	result, err := client.ListServicesWithLabel(TEST_NAMESPACE, labels)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 0 {
		t.Error("Service not filtered correctly")
	}

	labels = map[string]string{"name": "test"}
	result, err = client.ListServicesWithLabel(TEST_NAMESPACE, labels)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 1 {
		t.Error("Service not filtered correctly")
	}

}

func TestDeleteService(t *testing.T) {
	err := client.DeleteService(TEST_NAMESPACE, "testservice")
	if err != nil {
		t.Error(err)
	}

	result, err := client.ListServices(TEST_NAMESPACE)
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) != 0 {
		t.Error("Service not deleted")
	}
}

func TestDeleteReplicationController(t *testing.T) {
	result, _ := client.ListReplicationControllers(TEST_NAMESPACE)

	err := client.DeleteReplicationController(TEST_NAMESPACE, result.Items[0].Name)

	if err != nil {
		t.Error(err)
	}

	result, err = client.ListReplicationControllers(TEST_NAMESPACE)

	if err != nil {
		t.Error(err)
	}
	if len(result.Items) != 0 {
		t.Error("Replication Controller not deleted")
	}
}

func TestListNodes(t *testing.T) {
	listNodes(t)
}

func listNodes(t *testing.T) *v1.NodeList {
	result, err := client.ListNodes()
	if err != nil {
		t.Error(err)
	}

	if len(result.Items) == 0 {
		t.Error("No nodes returned")
	}

	return result
}

func TestGetNode(t *testing.T) {
	nodes := listNodes(t)
	nodeName := nodes.Items[0].Name

	node, err := client.GetNode(nodeName)

	if err != nil {
		t.Error(err)
	}

	if node.Name != nodeName {
		t.Error("Node not found")
	}
}

func TestUpdateNode(t *testing.T) {
	nodes := listNodes(t)
	nodeName := nodes.Items[0].Name

	node, err := client.GetNode(nodeName)
	if err != nil {
		t.Error(err)
	}

	node.Labels["update"] = "updated"
	updatedNode, err := client.UpdateNode(node)
	if err != nil {
		t.Error(err)
	}

	if updatedNode.Labels["update"] != "updated" {
		t.Error("Node not updated")
	}
}
