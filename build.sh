#!/bin/bash

# fail immediately
set -e
set -o pipefail

# save where we are coming from, that's where our sources are
SRCDIR=`pwd`

# create correct golang dir structure for this project
echo "Creating directory structure"
PACKAGE_PATH="${GOPATH}/src/bitbucket.org/amdatulabs/amdatu-kubernetes-go"
mkdir -p "${PACKAGE_PATH}"

# copy sources to new directory
echo "Copying sources"
cd "${SRCDIR}"
tar -cO --exclude .git . | tar -xv -C "${PACKAGE_PATH}"

# build
echo "Building..."
cd "${PACKAGE_PATH}"
go build -v
