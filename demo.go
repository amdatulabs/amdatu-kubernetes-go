package main

import (
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/api/v1"
	"bitbucket.org/amdatulabs/amdatu-kubernetes-go/client"
	"log"
)

const KUBERNETES_URL = "http://10.150.16.32:8080"
const TEST_NAMESPACE = "client-test"

var kubernetes = client.NewClient(KUBERNETES_URL, "", "")

func main() {
	createRc()
	listPods()
}

func createRc() {
	replicas := int32(1)
	labels := map[string]string{"name": "test"}
	rc := v1.ReplicationController{
		ObjectMeta: v1.ObjectMeta{Name: "test", Namespace: TEST_NAMESPACE, Labels: labels},
		Spec: v1.ReplicationControllerSpec{
			Selector: map[string]string{"name": "test"},
			Replicas: &replicas,
			Template: &v1.PodTemplateSpec{
				ObjectMeta: v1.ObjectMeta{
					Labels: map[string]string{"name": "test"},
				},

				Spec: v1.PodSpec{
					Containers: []v1.Container{{
						Name:  "nginx",
						Image: "nginx",
					}},
				},
			},
		},
	}

	createdRc, err := kubernetes.CreateReplicationController(TEST_NAMESPACE, &rc)

	if err != nil {
		panic(err)
	}

	log.Printf("Created replication controller %v\n", createdRc.Name)
}

func listPods() {
	pods, err := kubernetes.ListPods(TEST_NAMESPACE)
	if err != nil {
		panic(err)
	}

	for _, pod := range pods.Items {
		log.Println(pod.Name)
	}
}
